STAMP Unit Test Amplifier
=========================
STAMP Unit Test Amplifier is a set of microservices to automatically generate variants of pre-existing unit test cases manually written by developers. In this way you will have a tool to speed up the detection of  behavioral regressions.

## License
STAMP services are freely available under the [LGPL license](LICENSE).